rrootage (0.23a-14) unstable; urgency=medium

  * Team Upload
  * Update d/watch for upcoming release
  * Update Miriam email address
  * Set Rules-Requires-Root: no
  * Drop obsolete libgl1-mesa-dev alternative dependency
  * Rewrite d/copyright in DEP5
  * Bump Standards-Version to 4.7.0

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on libbulletml-dev.

 -- Alexandre Detiste <tchet@debian.org>  Wed, 20 Nov 2024 11:00:05 +0100

rrootage (0.23a-13) unstable; urgency=medium

  * Team upload.
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.
  * Move the package to salsa.debian.org.
  * Mark rrootage-data Multi-Arch: foreign.
  * Drop obsolete menu file.
  * Remove Dafydd Harries from Uploaders. (Closes: #965392)
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make.
    Thanks to Helmut Grohne for the patch. (Closes: #918332)

 -- Markus Koschany <apo@debian.org>  Sat, 23 Jan 2021 16:51:14 +0100

rrootage (0.23a-12) unstable; urgency=medium

  * Team upload.
  * Rebase 09_rrootage_make_highres_default.patch and enable it.
  * rrootage.6: Mention that high resolution mode is the default now.
    Thanks to Ivy Foster for the report.

 -- Markus Koschany <apo@debian.org>  Wed, 16 Dec 2015 10:58:26 +0100

rrootage (0.23a-11) unstable; urgency=medium

  * Team upload.
  * Fix Lintian warning binaries-have-file-conflict by using the symlink from
    /usr/share/doc/rrootage-data only once in the rrootage binary package.

 -- Markus Koschany <apo@debian.org>  Tue, 08 Dec 2015 00:15:02 +0100

rrootage (0.23a-10) unstable; urgency=medium

  * Team upload.
  * Use compat level 9 and require debhelper >= 9.
  * wrap-and-sort -sa.
  * d/control: Remove obsolete DM-Upload-Allowed field.
  * Declare compliance with Debian Policy 3.9.6.
  * Switch to source format 3.0 (quilt) and remove quilt build dependency.
  * Use canonical Vcs-URI.
  * Add missing ${misc:Depends} substvar to binary packages.
  * rrootage.desktop: Add keywords and a comment in German.
  * Switch to dh sequencer and rewrite debian/rules.

 -- Markus Koschany <apo@debian.org>  Mon, 30 Nov 2015 23:45:06 +0100

rrootage (0.23a-9) unstable; urgency=low

  * Apply patch from Jacob Nevins to mention -fullscreen option in man page.
    Closes: #541951.

 -- Dafydd Harries <daf@debian.org>  Tue, 18 Aug 2009 17:10:58 +0100

rrootage (0.23a-8) unstable; urgency=medium

  * Fixes buffer overflow due to wrong conversion between units.
    (Closes: #504229)

 -- Peter De Wachter <pdewacht@gmail.com>  Tue, 11 Nov 2008 16:04:23 +0100

rrootage (0.23a-7) unstable; urgency=low

  [ Barry deFreese ]
  * Remove XS- from VCS fields
  * Remove deprecated encoding field and make desktop more HIG compliant
  * Fix watch file regexp
  * Bump Standards Version to 3.7.3

  [ Miriam Ruiz ]
  * Added DM-Upload-Allowed tag to control to allow uploads from Debian
    Maintainers.

  [ Peter De Wachter ]
  * Fix URL macro in manpage.
  * Don't link against freeglut3 (not needed, rrootage uses only GL and GLU).
  * Compile with libbulletml-dev >= 0.0.6-3. Fixes FTBFS.
  * Add myself to Uploaders.

 -- Peter De Wachter <pdewacht@gmail.com>  Wed, 19 Mar 2008 00:51:47 +0100

rrootage (0.23a-6) unstable; urgency=low

  [Miriam Ruiz]
  * Handle patches with quilt.
  * Updated menu.
  * Added patches for high resolution.
  * Use external libbulletml library. Added build dependency: libbulletml-dev
  * Start in windowed mode.

 -- Miriam Ruiz <little_miry@yahoo.es>  Wed, 29 Aug 2007 21:37:59 +0200

rrootage (0.23a-5) unstable; urgency=low

  * Build-depend on freglut3-dev rather than libglut2-dev. Closes: #394502.
  * Bump standards version.
  * Use ${source:Version} rather than ${Source-Version} in dependency on -data
    package for binNMUability.

 -- Dafydd Harries <daf@debian.org>  Sat, 28 Oct 2006 16:26:08 +0100

rrootage (0.23a-4) unstable; urgency=low

  * Add patch from Martin Michlmayr to allow compilatoin with G++ 4.1.
    Closes: #357404.
  * Update my email address.

 -- Dafydd Harries <daf@debian.org>  Sun, 26 Mar 2006 20:04:38 +0100

rrootage (0.23a-3) unstable; urgency=low

  * Removed circular dependency between rrootage and rrootage-data.
    Closes: #339887.
  * Upgraded standards version to 3.6.2
  * Modified watch file.
  * Added .desktop file (thanks to Stephan Hermann <sh@sourcecode.de>).
    Closes: #348026.
  * Changed Maintainer field to Debian Games Team.

 -- Miriam Ruiz <little_miry@yahoo.es>  Sat, 17 Dec 2005 13:00:33 +0100

rrootage (0.23a-2) unstable; urgency=low

  * Execute debhelper on arch-specific packages only in the binary-arch
    target. Closes: #303205.

 -- Miriam Ruiz <little_miry@yahoo.es>  Wed, 7 Apr 2005 02:21:00 +0100

rrootage (0.23a-1) unstable; urgency=low

  * Initial package.
  * This resolves the ITP bug. Closes: #259062.
  * Modified src/screen.c to prevent a buffer overflow when loading textures.
  * Modified src/attractmanager.c to prevent a buffer overflow when handling
    the preferences file.

 -- Dafydd Harries <daf@muse.19inch.net>  Tue, 25 Jan 2005 05:05:31 +0000
